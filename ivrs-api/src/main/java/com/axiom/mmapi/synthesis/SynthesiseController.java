package com.axiom.mmapi.synthesis;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SynthesiseController {
	
	@Autowired
	private TextSpeechService textSpeechService;
	
    @GetMapping(value = "/synthesise")
    public void synthesise( HttpServletResponse response) throws IOException {
    	response.setContentType("audio/wav");
    	ServletOutputStream responseOutputStream = response.getOutputStream();
		responseOutputStream.write(textSpeechService.speech("Hello"));
		responseOutputStream.flush();
		responseOutputStream.close();
    }
}
