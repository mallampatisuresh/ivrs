package com.axiom.mmapi.synthesis;

import marytts.LocalMaryInterface;
import marytts.MaryInterface;

import marytts.util.data.audio.AudioPlayer;
import org.springframework.stereotype.Service;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.ByteArrayOutputStream;
import java.util.Set;

@Service("textSpeechService")
public class TextSpeechService {

    
    public byte[] speech(String text) {

        MaryInterface maryInterface = null;
        try {

            maryInterface = new LocalMaryInterface();
            Set<String> voices = maryInterface.getAvailableVoices();
            maryInterface.setVoice(voices.iterator().next());

            AudioInputStream audioInputStream = maryInterface.generateAudio(text);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, baos);

            return baos.toByteArray();
        } catch (Exception exc) {
            exc.printStackTrace();
            return null;
        }
    }
}